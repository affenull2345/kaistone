# KaiStone - unofficial KaiStore client
This is an experimental program to access KaiCloud APIs. It is
currently still under development, so don't expect much from it.

The app defaults to staging servers (https://api.stage.kaiostech.com) and also
requires a proxy at http://localhost:524 if it is not run as an app.

KaiStone requires a KaiOS account. It is possible to create one here:
<https://developer.stage.kaiostech.com/devlogin>. However, I do not recommend
creating accounts on the staging server because it is not an official server
and is used for internal testing purposes by KaiOS. Alternatively, you can
change the [server config](src/config.js) to point to
https://api.kaiostech.com and use a real account on the production servers.

It's also possible to edit the configuration while the app is open - just open
the JS console, change the properties in `configContainer.config` and reload
the app (`location.reload()`).

## Building
Build the program using npm:
```
npm install
npm run build:prod
```
Output files will be in the `dist` directory.

## How to use
### On a desktop (requires administrator access)
Set up a proxy that also serves the files in `dist`:
(sample configuration for nginx)
```
server {
	listen 524 default_server;
	listen [::]:524 default_server;

	server_name _;
	
	root /path/to/kaistone/dist; # Replace this

	location / {
		try_files $uri $uri/ @kaios;
	}
	location @kaios {
		proxy_pass https://api.stage.kaiostech.com;
	}
}
```
Don't forget to change this server configuration when you change the URLs in
[src/config.js](src/config.js).
To open KaiStone, just go to <http://localhost:524>.
Clicking on the left or right softkey text will simulate a key press.
The center softkey is equivalent to the Enter key.

### On a device or in Kaiosrt
Select `dist` in WebIDE/Kaiosrt as a packaged app and install, or
run [gerda-deploy](https://gitlab.com/affenull2345/gerda-deploy)
while inside `dist`.

#### Installing apps
Apps cannot be installed directly in Kaiosrt due to a bug in Kaiosrt itself,
but it is possible to install apps on a device if you set some preferences:

> **WARNING: The official KaiStore app might stop working after this!!!**

 1. Open Kaiosrt or WebIDE with a connected device
 2. Open 'Device Preferences'
 3. Type `apps.` into the search bar
 4. Append `app://kaistone.affe.null` to the comma-separated list in `apps.serviceCenter.allowedOrigins`
 5. Change `api.kaiostech.com` in `apps.token.uri` to the API URL in [src/config.js](src/config.js). By default the custom API URL is `api.stage.kaiostech.com`.
 6. App installation should work! (Not all apps in the staging store are real and installable, though).

![Screenshot - Required Preferences](Required-Preferences.png)

#### Save apps in OmniSD format to install them later
> **This is the only working method to install apps on GerdaOS or Kaiosrt.**
Press "Download" while viewing an app to download its package. The generated
package can be installed using `navigator.mozApps.mgmt.install` or with an
app like Wallace Toolbox or OmniSD. You can also unpack them and then install
them in Kaiosrt.

### Using the API request feature
The API request feature is used to send authorized requests to KaiCloud. The
path can be either a path relative to the API server root (e.g. /v3.0/apps) or
an absolute path (e.g. https://api.kaiostech.com/v3.0/apps). The absolute path
will most likely fail to work when used in a browser because of CORS
restrictions, but does work in Kaiosrt or on a device. To see the result of
the request, open the network tab of the debugger (on Firefox, the keyboard
shortcut is CTRL+SHIFT+E) or the console.

## Where to find out more about the KaiCloud REST API

 - The [docs](docs/)
 - KaiStore network activity: With a privileged-devtools-enabled phone, open
   the KaiStore, open "Main Process" in WebIDE and go to the network tab in
   the debugger. You should see lots of activity, including requests to KaiOS's
   API servers.
 - The Submission Portal's network activity (Note: there are many
   hidden features in the submission portal. After logging in, it is possible
   to go somewhere like <https://developer.kaiostech.com/subpo/#/bookmarks> or
   <https://developer.kaiostech.com/subpo/#/partner-management> or
   <https://developer.kaiostech.com/subpo/#/role-management> or
   <https://developer.kaiostech.com/subpo/#/account-management>. Find others in
   the JS source bundle. They should access some special administration APIs)
 - <https://fota2.stage.kaiostech.com> - Some kind of FOTA administration
   portal. It uses source maps, so you can view its source code using tools
   like the Firefox debugger. Normal accounts don't have access to its API, but
   the page still loads. **Now defunct - requests return 404**

# Licensing
The KaiStone app is licensed under the GNU General Public License, version
3.0 or later. The user interface in [src/ui](src/ui) is licensed under the
GNU Lesser General Public License, version 3.0 or later.
