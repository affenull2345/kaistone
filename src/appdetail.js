// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { registerView, enterView } from './ui/view'
import { PageView } from './ui/page'
import { load, install } from './backend/apps'
import { findIcon } from './icon-helper'
import JSZip from 'jszip'
import 'file-saver'
import './apps.css'
import './appdetail.css'

class AppDetailView extends PageView {
	constructor(id) {
		super();
		this.headerHidden = true;

		this.isInstalled = false;
		this.app = load(id);
		this.apptitle = document.createElement('div');
		this.apptitle.className = 'appdetail-title';
		this.apptitle.textContent = this.app.display;
		var icon = findIcon(this.app, 56);
		if(icon){
			this.apptitle.style.backgroundImage =
				'url(' + icon + ')';
		}
		/* The app title should not have padding around it */
		this.root.style.padding = '0px';
		this.subroot = document.createElement('div');
		this.subroot.style.padding = '5px';
		this.subroot.appendChild(
			document.createTextNode(this.app.description));
		this.btn = document.createElement('button');
		this.btn.textContent = this.softkeys[1];
		this.btn.onclick = function(){
			if(this.app.type == 'bookmark') open(this.app.url);
			else if(this.app.type == 'privileged' ||
				this.app.type == 'certified' ||
				this.app.type == 'web') this.installPkg();
			else if(this.app.type == 'hosted') this.installHosted();
			else console.log(
				'Unsupported ' + this.app.type + ' app');
		}.bind(this);
		this.subroot.appendChild(this.btn);
		this.root.appendChild(this.apptitle);
		this.root.appendChild(this.subroot);
		var detail;
		if(this.app.type == 'bookmark'){
			detail = this.app.url;
		}
		else {
			detail = 'Version: ' + this.app.version;
		}
		this.subroot.appendChild(document.createTextNode(detail));
		this.progress = null;
	}
	download() {
		if(this.app.type !== 'privileged' &&
			this.app.type !== 'certified' &&
			this.app.type !== 'web')
		{
			alert('Cannot download ' + this.app.type + ' apps!');
			return;
		}
		var pkg = this.app.package_path;
		alert('Downloading ' + pkg);
		window.session.si.request(
			window.session.token, 'GET', null, pkg
		).then(function(response){
			//window.location.assign(response.url);
			if(!response.blob){
				alert('Failed to download app: Response error');
				return;
			}
			console.log('Downloaded app');
			var pkg = new JSZip();
			pkg.file('metadata.json', JSON.stringify({
				version: 1 /* metadata.json format version */,
				manifestURL: this.app.manifest_url
			})).file('application.zip', response.blob);
			console.log('Zipped package');
			pkg.generateAsync({type: "blob"}).then(function(b){
				console.log('Saving blob', b);
				saveAs(b, this.app.name + '-pkg.zip');
			}.bind(this));
		}.bind(this)).catch(function(e){
			alert('Failed to download app: ' + e);
		});
	}
	reportProgress(value) {
		if(value == 100){
			this.apptitle.removeChild(this.progress);
			this.progress = null;
		}
		else {
			if(this.progress === null){
				this.progress = document.createElement('div');
				this.apptitle.appendChild(this.progress);
			}
			if(value < 0){
				this.progress.textContent = 'Failed';
				this.progress.style.color = 'red';
			}
			else {
				this.progress.textContent =
					'Installing... (' + value + '%)';
			}
		}
	}
	installPkg() {
		install(this.app, true, this.reportProgress.bind(this));
	}
	installHosted() {
		install(this.app, false, this.reportProgress.bind(this));
	}
	afterEnter() {
		super.afterEnter();
		this.btn.focus();
	}
	get title() {
		return this.app.display;
	}
	get softkeys() {
		return ['',
			this.isInstalled ? 'Open' :
			this.app.type == 'bookmark' ? 'Open' :
			this.app.paid ? 'Buy' :
			'Install', this.isInstalled ? 'Uninstall' : 'Download'];
	}
	get handlers() {
		return [{
			name: 'keydown',
			callback: function(e){
				if(e.key == 'SoftRight') this.download();
			}.bind(this)
		}];
	}
	static get id() {
		return 'appdetail';
	}
}

function initialize(){
	registerView(AppDetailView);
}

export { AppDetailView, initialize }
