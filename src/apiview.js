// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { registerView, enterView } from './ui/view'
import { PageView } from './ui/page'

class APIView extends PageView {
	constructor() {
		super();
		this.form = document.createElement('form');
		this.path = document.createElement('input');
		this.meth = document.createElement('input');
		this.body = document.createElement('input');
		this.path.placeholder = 'Path (e.g. /v3.0/categories)';
		this.meth.placeholder = 'Method (e.g. POST)';
		this.body.placeholder = 'JSON';
		var submit = this.submit.bind(this);
		this.root.onkeydown = function(e){
			if(e.key == 'Enter') submit();
		}
		this.form.appendChild(this.path);
		this.form.appendChild(this.meth);
		this.form.appendChild(this.body);
		this.root.appendChild(this.form);
		this.loading = document.createElement('div');
		this.loading.style.display = 'none';
		this.loading.textContent = 'Loading...';
		this.selected = 0;
		this.root.appendChild(this.loading);
	}
	get handlers() {
		return [
			{
				name: 'keydown',
				callback: function(e){
					if(e.key == 'ArrowDown'){
						if(this.selected == 0){
							this.meth.focus();
							this.selected++;
						}
						else if(this.selected == 1){
							this.body.focus();
							this.selected++;
						}
						else {
							this.path.focus();
							this.selected = 0;
						}
					}
					else if(e.key == 'ArrowUp'){
						if(this.selected == 2){
							this.meth.focus();
							this.selected--;
						}
						else if(this.selected == 1){
							this.path.focus();
							this.selected--;
						}
						else {
							this.body.focus();
							this.selected = 2;
						}
					}
				}.bind(this)
			}
		].concat(super.handlers);
	}
	submit() {
		if(this.meth.value == 'DELETE' && !confirm('Are you REALLY SURE that you want to DELETE ' + this.path.value + '?')) return;
		var bodyobj = null;
		if(this.body.value)
		{
			try {
				bodyobj = JSON.parse(this.body.value);
			} catch(e){
				console.error(e)
			}
		}
		window.session.si.request(window.session.token,
			this.meth.value, bodyobj, this.path.value).then(
			function(response){
				console.log(response);
			}).catch(function(error){
				console.error(error);
			});
	}
	showLoading() {
		this.loading.style.display = 'block';
	}
	hideLoading() {
		this.loading.style.display = 'none';
	}
	afterEnter() {
		super.afterEnter();
		if(this.selected == 1)
			this.meth.focus();
		else if(this.selected == 2)
			this.body.focus();
		else
			this.path.focus();
	}
	static get id() {
		return 'api';
	}
	get softkeys() {
		return ['', 'Send', ''];
	}
	get title() {
		return 'Send an API request';
	}
}

function initialize() {
	registerView(APIView);
}

export { APIView, initialize }
