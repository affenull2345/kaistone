function findIcon(app, preferredSize) {
	var sizes = Object.keys(app.icons);
	var bestSize = null, currentDiff = null;
	sizes.forEach(function(size){
		var diff = preferredSize - size;
		if(diff < 0) diff = -diff;
		if(currentDiff === null || diff < currentDiff){
			bestSize = size;
			currentDiff = diff;
		}
	});
	if(!bestSize){
		return null;
	}
	return app.icons[bestSize];
}

export { findIcon }
