// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { registerView, enterView } from './ui/view'
import { PageView } from './ui/page'
import { ServerInfo } from './backend/server'
import * as loginBackend from './backend/login'
import { config, flush } from './config'

class LoginView extends PageView {
	constructor() {
		super();
		this.form = document.createElement('form');
		this.form.action = 'javascript:';
		this.errors = {};
		this.username = document.createElement('input');
		this.password = document.createElement('input');
		this.password.type = 'password';
		this.username.placeholder = 'Username (Email)';
		this.password.placeholder = 'Password';
		this.username.oninput = this.validateAndClearError.bind(this,
			'username');
		this.password.oninput = this.validateAndClearError.bind(this,
			'password');
		this.form.appendChild(this.username);
		this.form.appendChild(this.password);
		/*this.apikey = document.createElement('input');
		this.apikey.type = 'apikey';
		this.apikey.placeholder = 'API Key';
		this.apikey.oninput = this.validateAndClearError.bind(this,
			'apikey');
		this.form.appendChild(this.apikey);*/
		var validateAndSubmit = this.validateAndSubmit.bind(this);
		this.root.onkeydown = function(e){
			if(e.key == 'Enter') validateAndSubmit();
		}
		this.root.appendChild(this.form);
		this.loading = document.createElement('div');
		this.loading.style.display = 'none';
		this.loading.textContent = 'Loading...';
		this.root.appendChild(this.loading);
	}
	get handlers() {
		return [{
			name: 'keydown',
			callback: function(e){
				/* Very simple navigation */
				if(e.key == 'ArrowDown')
					this.password.focus();
				else if(e.key == 'ArrowUp')
					this.username.focus();
			}.bind(this)
		}];
	}
	validateAndSubmit () {
		var valid = true;
		if(!this.username.value){
			this.showError('username', 'Username is required!');
			valid = false;
		}
		if(!this.password.value){
			this.showError('password', 'Password is required!');
			valid = false;
		}
		/*if(!this.apikey.value){
			this.showError('apikey', 'API Key is required');
			valid = false;
		}*/
		if(!valid) return;
		this.showLoading();
		var _this = this;
		var si = new ServerInfo();
		loginBackend.logInWithAccount(si, this.username.value,
			this.password.value)
		.then(function(sess){
			_this.hideLoading();
			window.session = sess;
			//config.api.key = _this.apikey.value;
			//flush();
			enterView('apps');
		}).catch(function(error){
			_this.hideLoading();
			_this.showError(null, error);
		});
	}
	showError(name, message) {
		if(name){
			if(this.errors[name])
				this.errors[name].textContent = message;
			else {
				this.errors[name] = document.createElement('p');
				this.errors[name].textContent = message;
				this.errors[name].style.color = 'red';
				if(this[name].nextSibling){
					this.form.insertBefore(
						this.errors[name],
						this[name].nextSibling);
				}
				else {
					this.form.appendChild(
						this.errors[name]);
				}
			}
		}
		else {
			if(this.error) this.error.textContent = message;
			else {
				this.error = document.createElement('p');
				this.error.textContent = message;
				this.error.style.color = 'red';
				this.form.appendChild(this.error);
			}
		}
	}
	showLoading() {
		this.loading.style.display = 'block';
	}
	hideLoading() {
		this.loading.style.display = 'none';
	}
	validateAndClearError (name) {
		if(this[name].value && this.errors[name]){
			this.errors[name].parentNode.removeChild(
				this.errors[name]);
			this.errors[name] = null;
		}
	}
	afterEnter() {
		super.afterEnter();
		/*if(config.api.key){
			this.apikey.value = config.api.key;
			this.validateAndSubmit();
		}
		this.apikey.focus();*/
		this.username.focus();
	}
	static get id() {
		return 'login';
	}
	get softkeys() {
		return ['', 'Log In', ''];
	}
	get title() {
		return 'Log In';
	}
}

function initialize(){
	registerView(LoginView);
}

export { LoginView, initialize }
