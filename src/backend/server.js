// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import * as hawk from 'fxa-js-client/client/lib/hawk'
import * as sjcl from 'sjcl'
import { config } from '../config'

function randomString(size){
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	var result = [];

	for (var i = 0; i < size; ++i) {
		result[i] = chars[Math.floor(Math.random() * chars.length)];
	}

	return result.join('');
}

class ServerInfo {
	request(token, meth, obj, path, headers){
		var url = config.server.url;
		var actpath, acturl = config.server.fakeUrl ||
			config.server.url;
		return new Promise(function(resolve, reject){
			var xhr = new XMLHttpRequest({mozSystem: true});
			if(path[0] == '/'){
				actpath = acturl + path;
				path = url + path;
			}
			else {
				actpath = path;
			}
			console.log(actpath, path);
			xhr.responseType = 'blob';
			xhr.open(meth, path, true);
			if(headers){
				Object.keys(headers).forEach(function(name){
					xhr.setRequestHeader(name,
						headers[name]);
				});
			}
			xhr.setRequestHeader('Kai-API-Version',
				config.api.version);
			xhr.setRequestHeader('Kai-Request-Info',
				'ct="wifi", rt="auto", utc="' +
				Date.now() + '", utc_off="1", ' +
				'mcc="' + config.net.mcc + '", ' +
				'mnc="' + config.net.mnc + '", ' +
				'net_mcc="' + config.net.mcc + '", ' +
				'net_mnc="' + config.net.mnc + '"');
			xhr.setRequestHeader('Kai-Device-Info',
				'imei="' + config.device.id +
				'", curef="' + config.device.variant + '"');
			xhr.setRequestHeader('User-agent', config.device.ua);
			if(token){
				var hawkcreds = {
					id: token.kid,
					algorithm: token.mac_algorithm,
					key: sjcl.codec.base64.toBits(
						token.mac_key)
				};
				var hawkinfo = {
					credentials: hawkcreds
				};
				if(obj){
					hawkinfo.payload = JSON.stringify(obj);
					hawkinfo.contentType =
						'application/json';
				}
				if(hawkcreds.algorithm == 'hmac-sha-256')
					hawkcreds.algorithm = 'sha256';
				else if(hawkcreds.algorithm == 'hmac-sha-1')
					hawkcreds.algorithm = 'sha1';
				var hdr = hawk.client.header(actpath,
					meth, hawkinfo);
				if(hdr.err) reject(hdr.err);
				xhr.setRequestHeader('Authorization',
					hdr.field);
			}
			xhr.onerror = function(){
				reject('Request failed');
			}
			xhr.onload = function(){
				if(xhr.status != 201 && xhr.status != 200){
					reject('Request failed with ' +
						xhr.status + ': ' +
						xhr.statusText);
					return;
				}
				var reader = new FileReader();
				if(xhr.getResponseHeader('Content-type')
					.includes('json'))
				{
					reader.onload = function(){
						resolve(JSON.parse(
							this.result));
					}
					reader.readAsText(xhr.response);
				}
				else {
					reader.onload = function(){
						resolve({
							url: this.result,
							blob: xhr.response
						});
					}
					reader.readAsDataURL(xhr.response);
				}
			}
			xhr.setRequestHeader('Content-type',
				'application/json');
			xhr.send(JSON.stringify(obj));
		});
	}
}

function initialize(){
}

export { ServerInfo, initialize }
