// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { getKaiOSMajorVersion } from '../config'
import { loadService } from './services'
var cache = {};

function store(app){
	cache[app.name] = app;
}
function load(id){
	return cache[id];
}
function initializeTokenProvider(srv){
	class TokenProvider extends lib_apps.TokenProviderBase {
		constructor(service, session){
			super(service.id, session);
		}
		getToken(type){
			console.log(`GetToken type=${type}`);
			return {
				keyId: window.session.token.kid,
				macKey: window.session.token.mac_key,
				tokenType: lib_apps.TokenType.ACCOUNT
			};
		}
	}
	srv.setTokenProvider(TokenProvider);
}
function install(app, packaged, reportProgress){
	reportProgress(0);
	console.log('Installing', app.manifest_url);
	var installFunc = packaged ? 'installPackage' : 'install';
	if(getKaiOSMajorVersion() == 3){
		installFunc = packaged ? 'installPackage' : 'installPwa';
		loadService('apps').then(function(srv){
			initializeTokenProvider(srv);
			srv[installFunc](app.manifest_url)
			.then(function(app){
				reportProgress(100);
				console.log('Installed!', app);
			}).catch(function(e){
				console.log('Error installing app',
					e);
				reportProgress(-1);
			});
		});
	}
	else if(!navigator.mozApps ||
		!navigator.mozApps[installFunc])
	{
		alert('Your system does not support app installation. '+
			'The manifest URL is ' + app.manifest_url);
	}
	else {
		navigator.mozApps[installFunc](app.manifest_url)
		.then(function(){
			reportProgress(100);
			console.log('Installed!');
		}).catch(function(e){
			alert('Failed to install app: ' + e.name +
				' ' + e.message);
			console.log('Failed', e);
			reportProgress(-1);
		});
	}
}

export { store, load, install }
