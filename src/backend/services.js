const coreScripts = [
	"shared/core.js",
	"shared/session.js"
];
const serviceScripts = {
	apps: [
		"apps/service.js"
	]
};

const DAEMON_PORT = 8081;

var session = null;

var services = {};

function loadScript(path){
	return new Promise(function(resolve, reject){
		var el = document.createElement('script');
		el.src = 'http://127.0.0.1:' + DAEMON_PORT + '/api/v1/' +
			path;
		el.onload = function(e){
			console.log('Script loaded', this.src);
			resolve();
		}
		el.onerror = function(e){
			console.error('Script loading error', this.src);
			reject();
		}
		document.head.appendChild(el);
	});
}

function loadCore(){
	if(session) return Promise.resolve();
	else return Promise.all(coreScripts.map(loadScript)).then(function(){
		console.log('All core scripts loaded.');
		return new Promise(function(resolve, reject){
			var _session = new lib_session.Session();
			var sessionParams = {
				onsessionconnected: function(){
					session = _session;
					console.log('Session connected');
					resolve();
				},
				onsessiondisconnected: function(){
					session = null;
					services = {};
					console.log('Session disconnected');
				}
			};
			console.log('Opening session');
			_session.open(
				'websocket',
				'localhost:' + DAEMON_PORT,
				'secrettoken',
				sessionParams, true);
		});
	});
}

function loadService(name){
	if(!serviceScripts[name]) return Promise.reject('Service not found');
	if(services[name]) return Promise.resolve(services[name]);
	return loadCore().then(function(){
		return Promise.all(serviceScripts[name].map(
			loadScript));
	}).then(function(){
		if(name == 'apps'){
			return lib_apps.AppsManager.get(session).then(srv => {
				console.log('Got AppsManager', srv);
				services[name] = srv;
				return Promise.resolve(srv);
			});
		}
		else {
			return Promise.resolve();
		}
	});
}

export { loadService }
