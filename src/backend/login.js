// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { config } from '../config'
import { setup } from 'fxa-js-client/client/lib/credentials'
import * as sjcl from 'sjcl'

class Session {
	constructor(si, token) {
		this.si = si;
		this.token = token;
	}
}

function encode(buf) {
	var pw = sjcl.codec.base64.fromBits(buf);
	console.log(pw);
	return pw;
}

function logInWithKey(si, apikey){
	return new Promise(function(resolve, reject){
		si.request(null, 'POST', {
			brand: config.device.brand,
			device_id: config.device.id,
			device_type: config.device.type,
			model: config.device.model,
			os: config.device.os,
			os_version: config.device.version,
			reference: config.device.variant
		}, '/v' + config.api.version + '/applications/' +
		config.app.id + '/tokens',
		{'Authorization': 'Key ' + apikey})
		.then(function(response){
			console.log(response);
			resolve(new Session(si, response));
		}).catch(function(error){
			reject(error);
		});
	});
}
function logInWithAccount(si, username, password){
	return new Promise(function(resolve, reject){
		setup(username, password).then(function(auth){
			return si.request(null, 'POST', {
				device: {
					brand: config.device.brand,
					device_id: config.device.id,
					device_type: config.device.type,
					model: config.device.model,
					os: config.device.os,
					os_version: config.device.version,
					reference: config.device.variant
				},
				application: {
					id: config.app.id
				},
				grant_type: 'password',
				password: encode(auth.authPW),
				scope: 'core',
				user_name: auth.emailUTF8
			}, '/v' + config.api.version + '/tokens')
		}).then(function(response){
			console.log(response);
			resolve(new Session(si, response));
		}).catch(function(error){
			reject(error);
		});
	});
}

function initialize(){
}

export { Session, logInWithKey, logInWithAccount, initialize }
