// KaiOS User interface library used in KaiStone
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { setTitle, setSoftkeys } from './panels'
import './view.css'
var views, cache, path, focused, usedIds, registeredHandlers = {};
var headerHidden;

function generateNonce(){
	var nonce, i;
	var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";
	do {
		for(nonce='', i = 0; i < 20; i++){
			nonce +=
				chars[Math.floor(Math.random() * chars.length)];
		}
	} while(usedIds[nonce]);
	usedIds[nonce] = true;
	return nonce;
}

window.addEventListener('keydown', function(e){
	if(e.key == 'Backspace' && path.length > 1){
		e.preventDefault();
	}
});
window.addEventListener('keyup', function(e){
	if(e.key == 'Backspace' && path.length > 1){
		leaveView();
		e.preventDefault();
	}
});

class View {
	constructor() {
		this.root = document.createElement('div');
		this.root.id = 'viewroot-' + this.constructor.id +
			generateNonce();
		this._headerHidden = false;
	}
	set headerHidden(hidden) {
		this._headerHidden = hidden;
		updateHeader(hidden);
	}
	get headerHidden() {
		return this._headerHidden;
	}
	beforeEnter() {}
	afterEnter() {}
	beforeLeave() {}
	afterLeave() {}
	get handlers() {
		return [];
	}
}

function updateHeader(hidden){
	if(headerHidden != hidden){
		if(hidden){
			document.body.classList.add('header-hidden');
		}
		else {
			document.body.classList.remove('header-hidden');
		}
		headerHidden = hidden;
	}
}

function registerView(cls){
	views[cls.id] = cls;
}
function getCurrentView(){
	return path[path.length-1];
}
function registerHandlers(view, viewid){
	registeredHandlers[viewid] = view.handlers;
	registeredHandlers[viewid].forEach(function(h){
		console.log('Adding handler ' + h.name + ' for ' +
			view.constructor.id);
		window.addEventListener(h.name, h.callback);
	});
}
function unregisterHandlers(view, viewid){
	registeredHandlers[viewid].forEach(function(h){
		console.log('Removing handler ' + h.name + ' for ' +
			view.constructor.id);
		window.removeEventListener(h.name, h.callback);
	});
	registeredHandlers[viewid] = [];
}
function enterView(id){
	var cur = getCurrentView();
	var idWithoutArgs = id.replace(/#.*$/, '');
	var arg = id.replace(/^[^#]*#/, '');
	if(cur){
		focused[cur.constructor.id] = document.activeElement;
		unregisterHandlers(cur);
	}
	if(!cache[id]) cache[id] = new views[idWithoutArgs](arg);
	cache[id].beforeEnter();
	updateHeader(cache[id]._headerHidden);
	document.activeElement.blur();
	setTitle(cache[id].title);
	setSoftkeys.apply(window, cache[id].softkeys);
	path.push(cache[id]);
	registerHandlers(cache[id]);
	cache[id].afterEnter();
}
function leaveView(){
	var oldcur = getCurrentView();
	oldcur.beforeLeave();
	path.pop();
	var cur = getCurrentView();
	setTitle(cur.title);
	setSoftkeys.apply(window, cur.softkeys);
	unregisterHandlers(oldcur);
	updateHeader(cur._headerHidden);
	oldcur.afterLeave();
	focused[cur.constructor.id].focus();
	registerHandlers(cur);
}
function initialize(){
	var h;
	views = {};
	cache = {};
	usedIds = {};
	focused = {};
	path = [];
	headerHidden = false;
	Object.keys(registeredHandlers).forEach(function(viewid){
		registeredHandlers[viewid].forEach(function(h){
			console.log('Removing leftover handler ' + h.name);
			window.removeEventListener(h.name, h.callback);
		});
	});
}

export { View, registerView, enterView, leaveView, getCurrentView, initialize }
