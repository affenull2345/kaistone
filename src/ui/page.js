// KaiOS User interface library used in KaiStone
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { View, getCurrentView } from './view'
import './page.css'

class PageView extends View {
	constructor() {
		super();
		document.getElementById('root').appendChild(this.root);
		this.root.classList.add('page-viewroot');
		this.root.classList.add('hidden-view');
	}
	beforeEnter() {
		var cv = getCurrentView();
		if(cv) cv.root.classList.add('hidden-view');
	}
	afterEnter() {
		this.root.classList.remove('hidden-view');
	}
	beforeLeave() {
		this.root.classList.add('hidden-view');
	}
	afterLeave() {
		var cv = getCurrentView();
		if(cv) cv.root.classList.remove('hidden-view');
	}
}

export { PageView }
