// KaiOS User interface library used in KaiStone
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { getCurrentView } from './view'
import { PageView } from './page'

class MenuView extends PageView {
	constructor() {
		super();
		this.items = [];
		this.navigationDisabled = false;
	}
	afterEnter() {
		super.afterEnter();
		if(this.items.length > 0) this.items[0].focus();
	}
	addItem(text, onclick, onfocus, onblur) {
		var el = document.createElement('button');
		el.className = 'menuitem';
		if(onclick) el.onclick = onclick;
		if(onfocus) el.onfocus = onfocus;
		if(onblur) el.onblur = onblur;
		el.textContent = text;
		this.root.appendChild(el);
		this.items.push(el);
		// If the view is the current and we have just added the first
		// item, then focus it!
		if(this.items.length == 1 && getCurrentView() == this)
			el.focus();
		return el;
	}
	get handlers() {
		return [
			{
				name: 'keydown',
				callback: function(e){
					if(this.navigationDisabled) return;
					var idx;
					if(this.items.length == 0) return;
					if(e.key == 'ArrowDown'){
						idx = this.items.indexOf(
							document.activeElement);
						idx++;
						if(idx >= this.items.length ||
							idx < 0)
						{
							idx=0;
						}
						this.items[idx].focus();
						e.preventDefault();
					}
					else if(e.key == 'ArrowUp'){
						idx = this.items.indexOf(
							document.activeElement);
						idx--;
						if(idx < 0){
							idx=this.items.length-1;
						}
						this.items[idx].focus();
						e.preventDefault();
					}
				}.bind(this)
			}
		].concat(super.handlers);
	}
}

export { MenuView }
