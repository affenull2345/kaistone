// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import * as backend from './backend'
import * as ui from './ui'
import * as views from './ui/view'
import * as loginview from './login'
import * as apiview from './apiview'
import * as appsview from './apps'
import * as appdetail from './appdetail'
import './app.css'

function initialize(){
	backend.initialize();
	ui.initialize();
	loginview.initialize();
	apiview.initialize();
	appsview.initialize();
	appdetail.initialize();
	views.enterView('login');
}

export { initialize }
