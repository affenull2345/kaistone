// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
import { registerView, enterView } from './ui/view'
import { MenuView } from './ui/menu'
import { store } from './backend/apps'
import { config } from './config'
import { findIcon } from './icon-helper'
import './apps.css'

class AppsView extends MenuView {
	constructor() {
		super();
		this.loadApps(1);
	}
	loadApps(pagenum) {
		this.navigationDisabled = true;
		this.showLoading();
		window.session.si.request(window.session.token,
			'GET', null, config.api.kc_ksfe_apps_url +
			'?bookmark=true&link=true&imei=' + config.device.id +
			'&page_size=' + config.page.size + '&page_num=' +
			pagenum + '&simMNC=' + config.net.mnc + '&simMCC=' +
			config.net.mcc + '&currentMCC=' + config.net.mcc +
			'&currentMNC=' + config.net.mnc
		).then(function(response){
			this.hideLoading();
			this.addApps(response, pagenum);
		}.bind(this)).catch(function(error){
			console.error(error);
		});
	}
	get handlers() {
		return [{
			name: 'keydown',
			callback: function(e){
				if(e.key == 'SoftLeft') enterView('api');
			}
		}].concat(super.handlers);
	}
	showLoading(){
		if(!this.loading){
			this.loading = document.createElement('p');
			this.loading.textContent = 'Loading...';
		}
		this.root.appendChild(this.loading);
		this.loading.scrollIntoView({block: 'end'});
	}
	hideLoading(){
		this.root.removeChild(this.loading);
	}
	addApps(response, pagenum){
		var lastel;
		response.apps.forEach(function(app){
			var el = this.addItem(app.display);
			store(app);
			el.classList.add('app');
			var icon = findIcon(app, 56);
			if(icon){
				el.style.backgroundImage = 'url(' + icon + ')';
			}
			el.textContent = app.display;
			if(app.subtitle){
				var st = document.createElement('div');
				st.className = 'app-subtitle';
				st.textContent = app.subtitle;
				el.appendChild(st);
			}
			el.onclick = function(){
				if(app.type == 'link') open(app.url);
				else enterView('appdetail#' + app.name);
			}
			this.root.appendChild(el);
			lastel = el;
		}.bind(this));
		if(lastel && !response.last_page){
			lastel.onfocus = function(e){
				e.target.onfocus = null; /* Don't load again */
				this.loadApps(pagenum+1);
			}.bind(this)
		}
		this.navigationDisabled = false;
	}
	get title() {
		return 'Apps';
	}
	get softkeys() {
		return ['API request', 'Select', ''];
	}
	static get id() {
		return 'apps';
	}
}

function initialize(){
	registerView(AppsView);
}

export { AppsView, initialize }
