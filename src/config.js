// KaiStone - free and open source KaiStore client
// Copyright (C) 2021 Affe Null
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
function recursiveUpdate(def, custom){
	Object.keys(custom).forEach(function(key){
		if(typeof custom[key] === 'object' &&
			typeof def[key] === 'object')
		{
			// def[key] is passed as a reference
			recursiveUpdate(def[key], custom[key]);
		}
		else {
			def[key] = custom[key];
		}
	});
	return def;
}
function extractChanged(def, mixed){
	var custom = {};
	Object.keys(mixed).forEach(function(key){
		if(typeof mixed[key] === 'object' &&
			typeof def[key] === 'object')
		{
			custom[key] = extractChanged(def[key], mixed[key]);
		}
		else if(def[key] !== mixed[key]){
			custom[key] = mixed[key];
		}
	});
	return custom;
}

class ConfigContainer {
	constructor(defaultValues) {
		this._def = defaultValues;
		var config = localStorage.getItem('config');
		if(config){
			try {
				config = JSON.parse(config);
			}
			catch(e){
				console.error("Invalid config", e);
				config = null;
			}
		}
		if(!config){
			// Deep clone
			this.config = JSON.parse(JSON.stringify(defaultValues));
		}
		else {
			this.config = recursiveUpdate(
				JSON.parse(JSON.stringify(defaultValues)),
				config);
		}
		window.addEventListener('beforeunload', this.flush.bind(this));
	}
	flush() {
		localStorage.setItem('config',
			JSON.stringify(extractChanged(this._def, this.config)));
	}
}

function getKaiOSVersion(){
	var kaiUA = /(KAIOS|B2GOS)\/([^ ]*)/.exec(navigator.userAgent);
	if(kaiUA) return kaiUA[2];
	else return '2.5.4';
}
function getKaiOSMajorVersion(){
	return Number(getKaiOSVersion().replace(/(\d*).*/, '$1'));
}

var container = new ConfigContainer({
	server: {
		url: (typeof navigator.b2g !== 'undefined' ||
			location.origin.indexOf('app://')==0) ?
			'https://api.stage.kaiostech.com' : // on device
			'http://localhost:524', // Proxy needed, no systemXHR
		fakeUrl: 'https://api.stage.kaiostech.com'
	},
	device: {
		brand: 'AlcatelOneTouch',
		id: '123456789012345',
		type: 999999,
		model: 'GoFlip2',
		os: 'KaiOS',
		version: getKaiOSVersion(),
		variant: '4044O-2BAQUS1-R',
		ua: 'Mozilla/5.0 (Mobile; GoFlip2; rv:48.0) Gecko/48.0 Firefox/48.0 KAIOS/' + getKaiOSVersion()
	},
	app: {
		service: 'wJUV7lQRRdooMc6WUyhS',
		id: 'lr9Cts0RhbaNRJ5i_gKf',
		name: 'KaiOS Plus',
		ver: '2.5.4'
	},
	api: {
		version: '3.0',
		kc_ksfe_apps_url: '/kc_ksfe/v1.0/apps'
	},
	net: {
		mcc: '0',
		mnc: '0'
	},
	page: {
		size: 10
	}
});
var flush = container.flush.bind(container);
console.log('Config: ', container.config);
window.configContainer = container; /* For debugging */
var config = container.config;
export { config, flush, getKaiOSVersion, getKaiOSMajorVersion }
