const JSZip = require('jszip');
const fs = require('fs');
const path = require('path');

var zip = new JSZip();

const dirpath = path.resolve(__dirname, 'dist');
const dir = fs.opendirSync(dirpath);

async function addFiles(){
	for await (const dirent of dir) {
		const buf = await fs.promises.readFile(
			path.resolve(dirpath, dirent.name));
		console.log(`Packing file ${dirent.name}...`);
		zip.file(dirent.name, buf);
	}
}

addFiles().then(function(){
	zip.generateAsync({type: 'nodebuffer'}).then(function(buf){
		var pkg = new JSZip();

		pkg.file('metadata.json', JSON.stringify({
			version: 1,
			manifestURL: 'app://kaistone.affe.null/manifest.webapp'
		})).file('application.zip', buf);
		pkg.generateAsync({type: 'nodebuffer'}).then(function(buf2){
			fs.writeFile(
				path.resolve(__dirname, 'kaistone.zip'),
				buf2, function(err){
					if(err) console.error(err);
					else console.log('Success');
				});
		});
	});
});
