# KaiCloud API documentation (incomplete)

## General information
### API endpoint locations
There are several places where Kai API endpoints are located.
Here is a list:

 - `/v3.0/` (most endpoints are here)
 - `/kc_ksfe/v1.0/apps` (probably the same as `/v3.0/apps`,
   possibly more available in `/kc_ksfe/v1.0/`)
 - `/kc_core/v2.0/accounts` (possibly more in `/kc_core/v2.0/`)
 - `/apps/manifest` (equivalent to `/v3.0/apps/manifest`)

These paths are available on every KaiOStech domain that uses the APIs:

 - <https://developer.kaiostech.com>
 - <https://developer.stage.kaiostech.com>
 - <https://developer.test.kaiostech.com>
 - <https://api.kaiostech.com>
 - <https://api.stage.kaiostech.com>
 - <https://api.test.kaiostech.com>
 - <https://fota2.stage.kaiostech.com>
 - <https://services.kaiostech.com>
 - <https://services.stage.kaiostech.com>
 - <https://services.test.kaiostech.com>
 - and possibly more

### Authorization

Most endpoints require Hawk authorization headers to access them. Except for
the ones used to retreive tokens, I have found only two that don't require
authorization: [`/v3.0/categories`](https://api.kaiostech.com/v3.0/categories)
and its Smart Touch counterpart,
[`/touch/v1.0/categories`](https://api.kaiostech.com/touch/v1.0/categories).

There are two ways of obtaining a token:

 - [using `/v3.0/tokens`](v3.0/tokens.md)
 - or [using per-application API keys](v3.0/applications.md).

The latter is used by KaiStore, but allows only very restricted access.

After obtaining a token object, the `mac_key` should be base64-decoded into
a buffer (if you use SJCL, the call would be
`sjcl.codec.base64.toBits(token.mac_key)`). Then, you can use a
[hawk implementation](https://www.npmjs.com/package/hawk) to obtain the
Authorization header:
```javascript
const Hawk = require('hawk');

function getAuthHeader(token, url, meth, jsonParams){
	var credentials = {
		id: token.kid,
		key: sjcl.codec.base64.toBits(token.mac_key)
		algorithm: 'sha256'
	};
	var hawkopts = {
		credentials: credentials
	};
	if(jsonParams){
		hawkopts.payload = JSON.stringify(jsonParams);
		hawkopts.contentType = 'application/json';
	}
	return Hawk.client.header(url, meth, hawkopts);
}
```

### Request format
Requests are sent in JSON format (`application/json`). There are also some
Kai-specific headers that identify the device
(but they are not always required):

 - `Kai-API-Version`: the current value is 3.
 - `Kai-Request-Info`: the device's network info. Format:
   `ct="[network type]", rt="[auto|manual]", utc="[Date.now()]",
   utc_off="[Timezone offset in hours]", mnc="[sim's mnc]", mcc="[sim's mcc]",
   net_mnc="[network's mnc]", net_mcc="[network's mcc]"` (mcc and mnc are null
   if sim not present)
 - `Kai-Device-Info`: the device information. Format:
   `imei="[IMEI]", curef="[Device CUREF/variant name]"`

For some requests, KaiCloud also requires a User agent with the KaiOS version
number (`KAIOS/2.5.1`, for example).
