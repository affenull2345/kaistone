# `/v3.0/accounts`

Authorization not required for PUT or POST

## Methods

 - GET (authorization required, not allowed for normal users)
 - POST (create new account)
 - PUT (reset password)

## POST parameters for creating a new account
TODO

## PUT parameters for resetting a password
TODO

## Response to POST after account creation
TODO

## Response to PUT after account creation
TODO
