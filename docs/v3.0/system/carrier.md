# `/v3.0/system/carrier`

Authorization required

## Methods

 - GET
 - POST (not allowed for normal users)

## Response

Responds with a list of partners/carriers. Each carrier is an object:

 - `carrier`: Probably always true
 - `categories`: Array of categories that are shown in the store (order is important!)
 - `created_at`: Date of creation, in microseconds since epoch.
 - `curefs`: List of device types / CUREFS that the carrier supports
 - `deleted`: Whether the carrier is deleted
 - `hni_codes`: List of HNI codes of the carrier
 - `id`: Internal ID of the carrier
 - `name`: Display name of the carrier (e.g. "Orange")
 - `updated_at`: Date of modification, in microseconds since epoch.
