# `/v3.0/system/roles`

Authorization required

## Methods

 - GET
 - POST (not allowed for normal users)

## Response

Responds with a list of user roles. Each role is an object:

 - `deleted`: Whether the role is deleted
 - `description`: Human-readable description of the role
 - `id`: Internal ID
 - `name`: Display name (e.g. "Standard Developer")

## Standard roles
This list is incomplete.

| Role name		| Description					|
| ---------		| -----------					|
| Standard Developer	| Is allowed to submit apps to the submission portal.|
| Standard Publisher	| Is allowed to publish apps without approval.	|
| Standard User		| Role for normal users. Does not imply developer permissions.|
| Standard Approver	| Is allowed to approve apps in the submission portal.|
| Partner Developer	| Developer with special permissions		|
| Campaign Creator	| Can create push campaigns			|
| Campaign Reviewer	| Can review (approve/fail) campaigns		|
