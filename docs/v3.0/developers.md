# `/v3.0/developers`

Authorization not required for POST

## Methods

 - GET (authorization required, no special permissions needed, lists developers)
 - POST (create new account)

## Response to GET
Responds with a list of developers. Each developer is an object:

 - `id`: Internal account ID
 - `deleted`: Whether the account is deleted
 - `is_active`: Whether the account is active
 - `account`: Account details: {
   - `account_type`: Account type ???
   - `country`: Country of the account (e.g. "us")
   - `email_is_valid`: Whether the `email` field is valid or present
   - `id`: Internal account ID
   - `is_developer`: Whether the account is a developer (always true)
   - `language`: Preferred language code (e.g. "en")
   - `login`: Login email of the account
   - `mobile_phone_is_valid`: Whether the mobile phone number field is valid or
     present
   - `partner_id`: [Partner/carrier ID](./system/carrier.md) of the user
   - `roles`: List of [roles](./system/roles.md) of the user
   - `second_email_is_valid`: Is the second email valid/present?
   - `second_mobile_phone_is_valid`: Is the second mobile phone valid/present?
   - `yob`: Year of birth, or `0` if not available

 }

## POST parameters for creating a new account
TODO

## Response to POST after account creation
TODO
