# `/v3.0/apps` and `/kc_ksfe/v1.0/apps`

Authorization required

## Methods

 - GET

## Query string parameters

Most of these are not required, but specifying some might return more apps

 - `bookmark`: Include bookmark apps (true or false)
 - `link`: Include link apps (true or false)
 - `imei`: Device's IMEI (show IMEI-filtered testing apps)
 - `simMCC`: SIM's Mobile Country Code (null if not present)
 - `simMNC`: SIM's Mobile Network Code (null if not present)
 - `currentMCC`: Current network's Mobile Country Code (null if not present)
 - `currentMNC`: Current network's Mobile Network Code (null if not present)
 - `simMCC2`: SIM2's Mobile Country Code (null if not present)
 - `simMNC2`: SIM2's Mobile Network Code (null if not present)
 - `currentMCC2`: SIM2 Current network's Mobile Country Code (null if not
   present)
 - `currentMNC2`: SIM2 Current network's Mobile Network Code (null if not
   present)
 - `type`: Connection type (e.g. wifi)
 - `build_number`: Device firmware version number (e.g. 17.00.17.01)
 - `cu`: Device's CUREF/variant name
 - `hardware`: Hardware type (e.g. qcom)
 - `os`: KaiOS version number (e.g. 2.5.1)
 - `product_model`: Device model name (e.g. Nokia\_8110\_4G)
 - `platform_build_id`: Software build date
 - `platform_version`: Gecko version (e.g. 48.0a2)
 - `software`: OS name and version (e.g. KaiOS\_2.5.1)
 - `locale`: Language (e.g. en-US)
 - `page_num`: Page number (starting with 1)
 - `page_size`: Results per page
 - `category`: Category code (see [v3.0/categories](./categories.md))

## Response
Responds with an app list.

 - `provider`: Should be "Kaiostech"
 - `version`: Currently "1"
 - `last_page`: Boolean: Whether the app list ends here
 - `plusInfo`: {
    - `version`: currently "2.4.6"

   }
 - `apps`: Array of app objects: {
    - `id`: Internal ID of the app
    - `version`: Version of the app
    - `manifest_url`: Mini-Manifest URL (pass to mozApps.install)
    - `type`: App type (hosted, web, privileged, certified, bookmark, link)
    - `name`: Internal app name (don't display!)
    - `display`: Name to display
    - `subtitle`: Subtitle to display under the name
    - `description`: Detailed description of the app
    - `priority`: Priority?
    - `supported_languages`: Array of supported locales
    - `icons`: List of icons, as in the manifest
    - `bgs`: List of backgrounds, similar to icon list. Display behind the name
      as a marketing banner
    - `screenshots`: Object/Dictionary: [screenshot ID] -> [URL]
    - `theme`: Theme color of the app
    - `silent`: ??
    - `hidden`: Boolean: Specifies whether to show the app in a list or not
    - `category`: Primary category code of the app
    - `category_list`: Array with the app's category codes
    - `status`: ??
    - `package_path`: URL of the app's zip package
    - `default_locale`: The app's default language
    - `paid`: Boolean: Specifies whether the app is paid or not

   }
