# `/v3.0/tokens`

Authorization not required

## Methods
Only POST is supported

## POST Parameters

 - `brand`: Device brand name (e.g. Nokia)
 - `device_id`: Device's IMEI as a string
 - `device_type`: ???
 - `model`: Device's model (e.g. "Nokia 8110 4G")
 - `os`: "KaiOS"
 - `os_version`: KaiOS version as a string
 - `reference`: Device's CUREF/variant name

## Response
Responds with a token object.

 - `kid`: Hawk key ID
 - `mac_algorithm`: Hawk algorithm, currently `hmac-sha-256`
 - `mac_key`: Base64-encoded Hawk key
