# `/v3.0/categories`

Authorization not required for normal GET

## Methods

 - GET
 - POST (requires special account permissions and authorization)

## GET Response
Responds with an array of category objects.

Category object format (any of these properties may be missing):

 - `code`: Category code
 - `type`: Category type???
 - `name`: Category name
 - `locales`: Object/Dictionary: [Locale ID] -> [Localized category name].
   Example: `"locales": {"en-US": "Social"}`
 - `children`: Array of subcategory codes (they might not be present in the
   list)

# `/v3.0/categories/admin`

Used by the submission portal to retreive a more detailed list of categories.

Authorization required

## Methods

 - GET
 - PUT (requires special account permissions)

## GET response
Returns an array of objects with the following properties:

 - `id`: The internal ID of the category
 - `name`: The category name
 - `type`: Category type???
 - `pid`: Partner ID
 - `locales`: Object/Dictionary: [Locale ID] -> [Localized category name].
   Example: `"locales": {"en-US": "Social"}`
 - `created_at`: Date of creation, in microseconds since epoch

# `/v3.0/categories/xxxxxxxxxxxxxxxxxxxx`

Used to retreive information about a single category, where
`xxxxxxxxxxxxxxxxxxxx` is the internal category ID. Not accessible by normal
users, probably because they are not the owner of the category.

## Methods

 - GET (requires user to be the owner of the category)
 - PUT (requires special account permissions)

## Response/request format
Unknown.
