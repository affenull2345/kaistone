**NOTE: This API does no longer work and has been abandoned.**

# `/fota/v1.0`
The FOTA endpoints listed here are only available on staging servers. They
are used by the FOTA portal at <https://fota2.stage.kaiostech.com>. Most of
them require special account permissions, so it is hard to understand their
purpose and the exact data format that they require and return.

# `/fota/v1.0/curefs`

## GET method
This should return a list of available CUREFs.

# `/fota/v1.0/softwares`

## GET method
This should return a list of available "Softwares".

# `/fota/v1.0/products`

## GET method
This should return a list of available products.

# `/fota/v1.0/producttypes`

## GET method
This should return a list of available product types.

# `/fota/v1.0/softwaretypes`

## GET method
This should return a list of available software types.

# `/fota/v1.0/whitelistitems`

## GET method
This should return a list of whitelisted MCCs and MNCs.

# `/fota/v1.0/blacklistitems`

## GET method
This should return a list of blacklisted MCCs and MNCs.

# `/fota/v1.0/devicegroups`

## GET method
This should return a list of device groups.

# `/fota/v1.0/eprojects`

## GET method
This is used to retreive a list of "Projects" from the server. For some reason,
this endpoint (or at least the GET method) is not protected using access
control, so anybody can get a list of projects. The "Projects" seem to describe
devices running KaiOS.
