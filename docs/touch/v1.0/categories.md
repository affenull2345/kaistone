# `/touch/v1.0/categories`
Used by the touchscreen KaiStore variant to get a list of categories and their
subcategories.

Authorization not required for normal GET

## Methods

 - GET

## GET Response
Responds with an array of category objects.

Category object format (any of these properties may be missing):

 - `code`: Category code
 - `type`: Category type???
 - `name`: Category name
 - `locales`: Object/Dictionary: [Locale ID] -> [Localized category name].
   Example: `"locales": {"en-US": "Social"}`
 - `children`: Array of subcategory codes (unlike [`/v3.0/categories`](../../v3.0/categories.md), the list does include these categories too.)
